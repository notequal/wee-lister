# Standard Library Imports
import datetime
import time

# Weewx Package Imports
from weewx.cheetahgenerator import SearchList
from weewx.tags import TimespanBinder
from weeutil.weeutil import TimeSpan


class RecordsSearch(SearchList):
    """A search list extension that retrieves a list of past records."""

    def __init__(self, generator):

        SearchList.__init__(self, generator)

        return None

    def get_extension_list(self, timespan, db_lookup):
        """
        """

        week_dt = datetime.date.fromtimestamp(timespan.stop)\
            - datetime.timedelta(days = 9)

        # Convert it to unix epoch time:
        week_ts = time.mktime(week_dt.timetuple())

        db_manager = db_lookup(None)
        records = db_manager.genBatchRecords(startstamp = week_ts)

        search_list_extension = {'records': records}

        return [search_list_extension]


