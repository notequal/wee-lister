from setup import ExtensionInstaller

def loader():
    return WeeListerInstaller()

class WeeListerInstaller(ExtensionInstaller):

    def __init__(self):

        super(WeeListerInstaller, self).__init__(
            version = '0.0.1',
            name = 'wee-lister',
            description = 'A WeeWX extension that lists database records '\
                + 'as well as hourly and daily aggregate lists.',
            author = 'Stanley Engle',
            author_email = 'stan.engle@gmail.com',
            config = {
                'StdReport': {
                    'WeeListerReport': {
                        'skin': 'WeeListerReport'
                    }
                }
            },
            files = [
                ('bin/user', ['bin/user/lister.py']),
                    ('skins/WeeListerReport', [
                        'skins/WeeListerReport/skin.conf',
                        'skins/WeeListerReport/lister.csv.tmpl',
                        'skins/WeeListerReport/daily.csv.tmpl',
                        'skins/WeeListerReport/hourly.csv.tmpl'])]
        )

        return None
